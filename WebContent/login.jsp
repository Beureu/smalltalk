<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Webchat - Login </title>
		<link rel="stylesheet" type="text/css" href="styles/pico.min.css">
	</head>
	<body>
		<%@include file="header.html" %>
		
		<main class="container">
			<form method="post" action="Login">
				<label for="pseudonyme">
					Pseudonyme
					<input type="text" name="pseudonyme" id="pseudonyme" placeholder="Pseudonyme..." required>
				</label>
				<input type="submit" value="Connect">
			</form>
		</main>
		
		<%@include file="footer.html" %>
	</body>
</html>