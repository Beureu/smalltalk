<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title> WebChat - In-room </title>
		<link rel="stylesheet" type="text/css" href="styles/pico.min.css">
		<script>
			let ws = new WebSocket("ws://localhost:8080/SmallTalk/ChatWebSocket");
			ws.onopen = function() { };
			
			ws.onmessage = function(message) {
				let target_div = document.getElementById('chatlog');
				let wrapper = document.createElement('div');
				wrapper.className = "grid";
				
				// let div_name = document.createElement('div');
				// wrapper.innerHTML = "To do";
				
				let div_message = document.createElement('div');
				wrapper.innerHTML = message.data;
				
				//wrapper.appendChild(div_name);
				wrapper.appendChild(div_message);
				target_div.appendChild(wrapper);
			};
			
			function sendMessage() {
				ws.send(document.getElementById("message").value);
				document.getElementById("message").value = "";
			}
			
			function closeSession() {
				ws.close();
				document.location.href="Login";
			}
		</script>
	</head>
	<body>
		<%@include file="header.html" %>
		
		<main class="container">
			<div id="chatlog">
			</div>
			<div class="grid">
				<input type="text" id="message" placeholder="Write a message..."/>
			</div>
			<div class="grid">
				<button type="submit" id="sendButton" onClick="sendMessage()"> Send </button>
				<button type="submit" id="sendButton" onClick="closeSession()"> Quit </button>
			</div>
		</main>
		
		<%@include file="footer.html" %>	
	</body>
</html>